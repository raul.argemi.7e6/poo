import com.sun.org.apache.xpath.internal.operations.Bool
import javafx.scene.effect.SepiaTone
import java.util.*

/*Exercici 1*//*

class Estudiantes(val name: String, val textGrade: String) {
    override fun toString(): String {
        return "Student{name='$name', textGrade=$textGrade}"
    }
}

fun main(args: Array<String>) {
    val estudiante1 = Estudiantes("Mar", "FAILED")
    val estudiante2 = Estudiantes("Joan", "EXCELLENT")
    println(estudiante1.toString())
    println(estudiante2.toString())
}*/

/*//Exercici 2
enum class RainbowColor {
   VERMELL,
   TARONJA,
   GROC,
   VERDE,
   BLAU,
   VIOLETA;

   companion object {
      fun isRainbowColor(colorStr: String): Boolean {
         for (color in values()) {
            if (color.name.equals(colorStr, true)) {
               return true
            }
         }
         return false
      }
   }
}

fun main() {
   print("Introdueix un color: ")
   val colorStr = readLine() ?: ""
   val isInRainbow = RainbowColor.isRainbowColor(colorStr)
   println("Esta a l'arcoiris: $isInRainbow")
}*/
/*//Exercici3

open class Instrument{
    open fun makeSounds(vegades: Int){}

}
class Drump(private val tono: String): Instrument(){
    fun rebreSorolls(): Any {
        return if(tono == "A"){
            "TAAAM"
        }else if (tono == "O"){
            "TOOOM"
        }else if (tono == "U"){
            "TUUUM"
        }else{
            "To no trobat."
        }
    }
    override fun makeSounds(vegades: Int){
        repeat(vegades){
            println(rebreSorolls())
        }
    }
}

class Triangle(private val resonancia: Int): Instrument(){
    override fun makeSounds(vegades: Int){
        val so = "T${"I".repeat(resonancia)}NC"
        repeat(vegades){ println(so) }
    }
}

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}*/

//Exercici 4

/*
fun main() {
    val pregunta1 = FreeTextQuestion("Quina es la capital d'Espanya?", "Madrid")
    val pregunta2 = MultipleChoiceQuestion("Quan son 1+1?\nA: 2\nB: 3\nC: 4", Choice.A.letter)

    val quiz = Quiz(pregunta1, pregunta2)
    val correctes = quiz.solve()
    println("S'han respost un total de $correctes preguntes correctament")
}


abstract class Question {
    abstract val questionText: String
    abstract val correctAnswer: String
}

class FreeTextQuestion(override val questionText: String, override val correctAnswer: String) : Question()

class MultipleChoiceQuestion(override val questionText: String, override val correctAnswer: String) : Question()


class Quiz(vararg questions: Question) {
    private val questionList = mutableListOf<Question>()

    init {
        for (question in questions) {
            questionList.add(question)
        }
    }

    fun showQuestions() {
        questionList.forEach { println(it.questionText) }
    }

    fun solve(): Int {
        var correctQuestions = 0
        for (question in questionList) {
            println(question.questionText)
            println("Introdueix la teva resposta")
            val answer = readln()
            if (question is FreeTextQuestion) {
                if (answer == question.correctAnswer) correctQuestions++
            } else if (question is MultipleChoiceQuestion) {
                if (answer.uppercase() == question.correctAnswer) correctQuestions++
            }

        }
        return correctQuestions
    }
}

enum class Choice(val letter: String) {
    A("A"),
    B("B"),
    C("C")
}

*/

//Exercici 5

interface GymControlReader {
    fun nextId(): String
}

class GymControl(val reader: GymControlReader) {

    val userStatus = mutableMapOf<String, Boolean>()

    fun run() {
        for (i in 1..8) {
            val id = reader.nextId()
            if (userStatus.containsKey(id) && userStatus[id] == true) {
                println("$id Sortida")
                userStatus[id] = false
            } else {
                println("$id Entrada")
                userStatus[id] = true
            }
        }
    }
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {

    override fun nextId() = scanner.next()

}

fun main() {
    val scanner = Scanner(System.`in`)
    val reader = GymControlManualReader(scanner)
    val gymControl = GymControl(reader)
    gymControl.run()
}





















